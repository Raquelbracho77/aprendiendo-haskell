* Introducción a la programación funcional
* Definiciones elementales de funciones

** Media de 3 números
Definir la función *media3* tal que (media3 x y z) es la aritmética de los números *x, y* y *z*.

#+begin_src haskell
media3 x y z = (x+y+z)/3
media3 3 5 6
#+end_src

#+RESULTS:
: 4.666666666666667

** Suma de euros de una colección de monedas
Definir la función sumaMonedas tal que (sumaMonedas a b c d e) es la suma de los euros
correspondientes a monedas de 1 euro, b de 2 euros, c de 5 euros, d de 10 euros y e
de 20 euros:

#+begin_src haskell
sumaMonedas a b c d e = 1*a+2*b+5*c+10*d+20*e
sumaMonedas 0 0 0 0 1
#+end_src

#+RESULTS:
: 20

** Volumen de la esfera
Definir la función volumenEsfera tal que (volumenEsfera r) es el volumen de la
esfera de radio r. Por ejemplo, 

#+begin_src haskell
volumenEsfera r = (4/3)*pi*r^3
volumenEsfera 10
#+end_src

#+RESULTS:
: 4188.790204786391

** Área de ua corona circular
Definir la fn de areaDeCoronaCircular tal que (areaDeCoronaCircular r1 r2) es el 
área de una corona circular de radio interior r1 y radio exterior r2. 

 #+begin_src haskell
areaDeCoronaCircular r1 r2 = pi*(r2^2 - r1^2)
areaDeCoronaCircular 4 5
 #+end_src

 #+RESULTS:
 : 28.274333882308138

* Listas

** Funciones básicas para operar con listas

[[file:./img/dibujo-lista.png]]

*Para utilizar las funciones head, tail. last e init no*
*deben usarse listas vacías*

- Las siguientes funciones retornan como la forma "externa"
de una lista:

*** head
Toma una lista y devuelve su *cabeza*, es decir, el primer elemento

#+begin_src haskell
head [1,2,3,4,5]
#+end_src

#+RESULTS:
: 1

*** tail
Toma una lista y devuelve su cola, es decir corta la cabeza o primer elemento de la lista

#+begin_src haskell
tail [1,2,3,4,5]
#+end_src

#+RESULTS:
| 2 | 3 | 4 | 5 |

*** Last
Devuelve el último elemento de una lista

#+begin_src haskell
last [1,2,3,4,5]
#+end_src

#+RESULTS:
: 5

*** init 
Toma una lista y devuelve toda la lista excepto su último elemento

#+begin_src haskell
init [1,2,3,4,5]
#+end_src

#+RESULTS:
| 1 | 2 | 3 | 4 |

- Estas funciónes retornan la forma "interna" de una lista:

*** length
Devuelve el *tamaño* de una lista

#+begin_src haskell
length [5,4,3,2,1,0]
#+end_src

#+RESULTS:
: 6

*** null
*Comprueba* si una lista está vacía, si lo está, devuelve *True*, 

#+begin_src haskell
null [0,1,2]
#+end_src

#+RESULTS:
: False

#+begin_src haskell
null []
#+end_src

#+RESULTS:
: True

*** reverse
Muestra la lista alrevés.

#+begin_src haskell
reverse [9,8,7,6,5,4,3,2,1,0]
#+end_src

#+RESULTS:
| 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |

*** take 
Extrae el número de elementos que le asignamos para mostrar de la lista, por ejemplo:

#+begin_src haskell
take 2 [5,4,3,2,1]
#+end_src

#+RESULTS:
| 5 | 4 |

#+begin_src haskell
take 4 [1,2,4,5,6,76,55]
#+end_src

#+RESULTS:
| 1 | 2 | 4 | 5 |

*** drop
Muestra el resultado a partir de la cantidad de elementos que se le ordene.
Es decir, quita un número de elementos del comienzo de la lista.

#+begin_src haskell
drop 2 [33,3434,66,1,9]
#+end_src

#+RESULTS:
| 66 | 1 | 9 |

#+begin_src haskell
drop 5 [1,2,3,4,5,6,7,8,9,0]
#+end_src

#+RESULTS:
| 6 | 7 | 8 | 9 | 0 |

#+begin_src haskell
drop 20 [1,3]
#+end_src

#+RESULTS:
: []
 + En este caso, la lista que entregó es vacía ya que el número
como parámetro supera el nro. de elementos dentro de la lista,
comprobandose que *2* > *20* y dos son los elementos en la lista.
Donde 2 no es mayor que 20.

#+begin_src haskell
2 > 20
#+end_src

#+RESULTS:
: False

*** maximum
Devuelve el mayor elemento

#+begin_src haskell
maximum [1,2,3,70]
#+end_src

#+RESULTS:
: 70

*** minimum
Devuelve el menor elemento

#+begin_src haskell
minimum [1,2,3,70]
#+end_src

#+RESULTS:
: 1

*** sum 
Toma una lista de números y devuelve su suma

#+begin_src haskell
sum [40,60,1]
#+end_src

#+RESULTS:
: 101

*** product
devuelve el producto o multiplicación de la lista.

#+begin_src haskell
product [30,1,103.10]
#+end_src

#+RESULTS:
: 3093.0

*** elem
Toma un elemento "cualquiera" y verifica si está dentro de la lista.

[[file:./img/elem.png]]

#+begin_src haskell
4 `elem` [2,7,1,4,5]
#+end_src

#+RESULTS:
: True



** Rotación del listas
Definir la función rota1 tal que (rota1 xs) es la lista obtenida poniendo el
primer elemento de xs al final de la lista

#+begin_src haskell
rota1 xs = tail xs ++ [head xs]
rota1 [1,2,3,4,5]
#+end_src

#+RESULTS:
| 2 | 3 | 4 | 5 | 1 |


Definir la funcion rota tal que (rota n xs) es la lista obtenida poniendo
los n primeros elementos de xs al final de la lista

#+begin_src haskell
rota n xs = drop n xs ++ take n xs
rota 7 [5,4,3,2,1]
#+end_src

#+RESULTS:
| 5 | 4 | 3 | 2 | 1 |

** Rotación de listas con xor

#+begin_src haskell
xor3 x y = (x || y) && not (x && y)
xor3 True False
#+end_src

#+RESULTS:
: True

#+begin_src python :session pruebas
def xor3(x,y):
    return (x or y) and not (x and y)
#+end_src

#+RESULTS:

#+begin_src python :session pruebas :results output
print(xor3(True,False))
#+end_src

#+RESULTS:
: True

** Finales de una lista
Definir la fn finales tal que (finales n xs) es la lista formada por los
n finales elementos de xs

#+begin_src haskell
finales n xs = drop (length xs - n) xs
finales 3 [1,2,3,4,5]
#+end_src

#+RESULTS:
| 3 | 4 | 5 |

** Segmentos de una lista
Definir la fn segmento tal que (segmento m n xs)
es la lista de los elementosde xs comprendidos
entre la pisicion m y n.

#+begin_src  haskell
segmento m n xs = drop (m-1) (take n xs)
segmento 3 4 [3,4,1,2,7,9,0]
#+end_src

#+RESULTS:
| 1 | 2 |

** Extremos de una lista
Definir la función extremos tal que (extremos n xs)
es la lista formada por los n primeros elementos de
xs y los n finales elemetos de xs.

#+begin_src haskell
extremos n xs = take n xs ++ drop (length xs - n) xs
extremos 2 [1,2,3,4,5,6,7,8,9,0,10,11,12,13]
#+end_src

#+RESULTS:
| 1 | 2 | 12 | 13 |

** Mediano de 3 números
Definir la fn mediano tal que (mediano x y z) es el 
nro mediano de los tres nros x, y y z.

#+begin_src haskell
mediano x y z = x + y + z - minimum [x,y,z]-maximum [x,y,z]
mediano 3 2 5
#+end_src

#+RESULTS:
: 3

** Igualdad y diferencia de 3 elementos
Definir la fn tresIguales tal que (tresIgual x y z)
se verifica si los elementos son iguales

#+begin_src haskell
tresIguales x y z = x == y && y  == z
tresIguales 4 4 4
#+end_src

#+RESULTS:
: True

#+begin_notes
Para la diferencia, el resultado sería
False
#+end_notes

** Igualdad de 4 elementos
Definir la fn cuatroIguales tal que (cuatroIguales x y z u) se verifica
si los elementos x,y,z y u son iguales

#+begin_src haskell
cuatroIguales x y z u = x == y && tresIguales y z
tresIguales x y z = x == y && y == z 
cuatroIguales 5 5 5 5
tresIguales 4 4 4 
#+end_src

#+RESULTS:
: True

** Propiedad triangular
Las longitudes de los lados de un triangulo no pueden ser cualesquiera. 
Para que pueda construirse el triangulo, tiene que cumplirse la
la propiedad triangular; es decir, longitud de cada lado tiene que ser
menos que la suma de los otros dos lados.
Definir la fn triagular tal que (triangular a b c) se verifica si a,
b y c cumplen la propiedad triangular:

[[file:./img/triangulo.png]]

#+begin_src haskell
triangular a b c = a < b+c && b < a+c && c < a+b
triangular 3 4 5
#+end_src

#+RESULTS:
: True

#+begin_quote
3 < 4+5
True

4 < 3+5
True

5 < 3+4
True
#+end_quote


#+begin_src haskell
triangular a b c = a < b+c && b < a+c && c < a+b
triangular 30 4 5
#+end_src

#+RESULTS:
: False

#+begin_quote
30 < 4+5
False

4 < 30+5
True

5 < 30+4
True
#+end_quote


#+begin_src haskell
triangular a b c = a < b+c && b < a+c && c < a+b
triangular 3 40 5
#+end_src

#+RESULTS:
: False

#+begin_quote
3 < 40+5
True

40 < 3+5
False

5 < 3+40
True
#+end_quote

** División segura
Definir la fn divisionSegura tal que (divisionSegura x y) es x/y si y no es 
cero y 9999 en caso contrario.

#+begin_src haskell
divisionSegura x y = x/y
divisionSegura 7 2
#+end_src

#+RESULTS:
: 3.5

** Disyunción excluyente

La disyunción excluyente *xor* de dos fórmulas se verifica si una es verdadera y 
la otra es falsa.

 1. Definir la fn *xor1* que calcule la disyunción excluyente a partir de la tabla de
verdad. Usar 4 ecuaciones, una por cada línea de la tabla.

| xor1 True True = False    |
| xor1 True False = True    |
| xor1 False True = True   |
| xor1 False False = False |

 2. Definir la fn *xor2* que calcule la disyunción excluyente a partir
de la tabla de verdad y patrones. Usar 2 ecuaciones, una por cada valor
del primer argumento.

| xor2 True y = not y |
| xor2 False y = y    |

#+begin_src haskell
xor2 True y = not y
#+end_src

#+RESULTS:
: <interactive>:27:1-3: error:
:     • Variable not in scope: xor :: Bool -> [Char] -> t
:     • Perhaps you meant one of these:
:         ‘or’ (imported from Prelude), ‘Ghci19.xor2’ (imported from Ghci19),
:         ‘Ghci21.xor2’ (imported from Ghci21)

3. Definir la fn *xor3* que calcule la disyunción excluyente a partir de 
la *disyunción (||)*, *conjución (&&)* y *negación (not)*. Usar 1 ecuación.

| xor3 x y = (x || y) && not (x && y)   |

#+begin_src haskell
xor3 x y = (x||y) && not (x && y)
xor3 4 5
#+end_src

#+RESULTS:
: <interactive>:30:6: error:
:     • No instance for (Num Bool) arising from the literal ‘4’
:     • In the first argument of ‘xor3’, namely ‘4’
:       In the expression: xor3 4 5
:       In an equation for ‘it’: it = xor3 4 5

4. Definir la función *xor4* que calcule la disyunción excluyente a partir de 
*desigualdad (/=)*. Usar 1 ecuación.

#+begin_src haskell
xor4 x y = y = x /= y
xor4 3 2
#+end_src

#+RESULTS:
: <interactive>:35:1-4: error:
:     • Variable not in scope: xor4 :: Integer -> Integer -> t
:     • Perhaps you meant one of these:
:         ‘Ghci19.xor2’ (imported from Ghci19),
:         ‘Ghci21.xor2’ (imported from Ghci21), ‘xor2’ (line 26)


** Rotación de listas
Definir la fn *rota1* tal que (rota1 xs) es la lista obtenida poniendo el primer 
elemento de xs al final de la lista.

#+begin_src haskell
rota1 xs = tail xs ++ [ head xs]
rota1 [8,8,90,5,2]
#+end_src

#+RESULTS:
| 8 | 90 | 5 | 2 | 8 |

Definir la fn rota tal que (rota n xs) es la lista obtenida poniendo los n primeros
elementos de xs al final de la lista.

#+begin_src haskell
rota n xs = drop n xs ++ take n xs
rota 1 [1,2,3,4,5,6,7,8,9,0]
#+end_src

#+RESULTS:
| 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 0 | 1 |

#+begin_src haskell
rota n xs = drop n xs ++ take n xs
rota 3 [1234, 100, 30, 19]
#+end_src

#+RESULTS:
| 19 | 1234 | 100 | 30 |

** Rango de una lista
#+begin_src haskell
[1..20]
#+end_src

#+RESULTS:
| 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19 | 20 |

** Finales de una lista
Definir la función finales tal que (finales n xs) es la lista 
formada por los n finales elementos de xs.

#+begin_src haskell
finales n xs = drop (length xs -n) xs
finales 3 [1..5]
#+end_src

#+RESULTS:
| 3 | 4 | 5 |

** segmentos de una lista

Definir la fn segmento tal que (segmento m n xs) es la lista de los
elementos de xs comprendidos entre las posiciones m y n.

#+begin_src haskell

#+end_src
